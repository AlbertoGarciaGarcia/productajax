package com.products.Dto;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.Calendar;

@Entity
@Table(name="products")
public class Products {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "detail")
	private String detail;
	 @Column(name = "created_at")
	@Temporal(TemporalType.DATE)
	    private Calendar created_at;
	 @Column(name = "updated_at")
		@Temporal(TemporalType.DATE)
		    private Calendar updated_at;
	 
	 
	 
	 
	 
	 public Products() {
		 
	 }
	 public Products(int id,String name,String detail,Calendar created_at,Calendar updated_at) {
		 this.id = id;
		 this.name = name;
		 this.detail = detail;
		 this.created_at = created_at;
		 this.updated_at = updated_at;
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public Calendar getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Calendar created_at) {
		this.created_at = created_at;
	}
	public Calendar getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Calendar updated_at) {
		this.updated_at = updated_at;
	}
	 
	 
	 
	 
	 
	 
	 
	@Override
	public String toString() {
		return "Articulos [id=" + id + ", nombre=" + name + ", detail=" + detail + ", created_at=" + created_at + "]";
	}
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
}
