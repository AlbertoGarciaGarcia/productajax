package com.products.Dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.products.Dto.Products;



public interface ProductsDao extends JpaRepository<Products, Integer> {

}
