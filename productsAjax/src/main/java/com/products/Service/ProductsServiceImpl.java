package com.products.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.products.Dto.Products;
import com.products.Dao.ProductsDao;

@Service
public class ProductsServiceImpl implements ProductsService {
	
	@Autowired
	ProductsDao ProductsDao;
	@Override
	public List<Products> listarProductss() {
		// TODO Auto-generated method stub
		return ProductsDao.findAll();
	}

	@Override
	public Products guardarProducts(Products Products) {
		// TODO Auto-generated method stub
		return ProductsDao.save(Products);
	}

	@Override
	public Products ProductsXID(int id) {
		// TODO Auto-generated method stub
		return ProductsDao.findById(id).get();
	}

	@Override
	public Products actualizarProducts(Products Products) {
		
		return ProductsDao.save(Products);
	}

	@Override
	public void eliminarProducts(int id) {
		ProductsDao.deleteById(id);
		
	}

}
