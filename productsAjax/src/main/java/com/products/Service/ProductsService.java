package com.products.Service;

import java.util.List;

import com.products.Dto.Products;


public interface ProductsService {
public List<Products> listarProductss(); //Listar All 
	
	public Products guardarProducts(Products Products);	//Guarda un Products CREATE
	
	public Products ProductsXID(int id); //Leer datos de un Products READ
	
	public Products actualizarProducts(Products Products); //Actualiza datos del Products UPDATE
	
	public void eliminarProducts(int id);// Elimina el Products DELETE
}
