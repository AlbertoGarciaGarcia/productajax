package com.products.Controller;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.products.Dto.Products;
import com.products.Service.ProductsServiceImpl;



@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
@RequestMapping("/api")
public class ProductsController {

	@Autowired
	ProductsServiceImpl ProductsServideImpl;
	
	@GetMapping("/Products")
	public List<Products> listarProducts(){
		return ProductsServideImpl.listarProductss();
	}
	
	@PostMapping("/Products")
	public Products salvarProducts(@RequestBody Products Products) {
		
		return ProductsServideImpl.guardarProducts(Products);
	}
	
	@GetMapping("/Products/{id}")
	public Products ProductsXID(@PathVariable(name="id") int id) {
		
		Products Products_xid= new Products();
		
		Products_xid=ProductsServideImpl.ProductsXID(id);
		
		System.out.println("Products XID: "+Products_xid);
		
		return Products_xid;
	}
	
	@PutMapping("/Products/{id}")
	public Products actualizarProducts(@PathVariable(name="id")int id,@RequestBody Products Products) {
		
		Products Products_seleccionado= new Products();
		Products Products_actualizado= new Products();
		
		Products_seleccionado= ProductsServideImpl.ProductsXID(id);
		
		Products_seleccionado.setName(Products.getName());
		Products_seleccionado.setDetail(Products.getDetail());
		Products_seleccionado.setCreated_at(Products.getCreated_at());
		Products_seleccionado.setUpdated_at(Products.getUpdated_at());
		
		Products_actualizado = ProductsServideImpl.actualizarProducts(Products_seleccionado);
		
		System.out.println("El Products actualizado es: "+ Products_actualizado);
		
		return Products_actualizado;
	}
	
	@DeleteMapping("/Products/{id}")
	public void eleiminarProducts(@PathVariable(name="id")int id) {
		ProductsServideImpl.eliminarProducts(id);
	}

	
}
